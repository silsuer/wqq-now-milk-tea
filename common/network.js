import AppConfig from '../config/index.js';
import { User } from '../models/user.js';

const publicApis = {
  '/api/1.0/login': 1,
};

let pendingRequestQueue = [];

export const Network = {
  event: {
    loginSuccess: 'loginSuccess',
  },
  request(params) {
    const { url: api } = params;

    let copyParams = Object.assign({}, params);
    copyParams.url = AppConfig.serverHost + api;
    if (publicApis[api] == 1) {
      return new Promise((resolve, reject) => {
        uni.request({
          ...copyParams,
          success: (resp) => {
            resolve(resp);
          },
          fail: (err) => {
            reject(err);
          },
        });
      });
      // return uni.request(copyParams);
    }
    let targetHeader = Object.assign({}, copyParams.header, {
      Authorization: 'Bearer ' + User.token,
      Version: AppConfig.version,
    });
    copyParams.header = targetHeader;
    if (User.isPreLogin()) {
      return new Promise((resolve, reject) => {
        uni.request({
          ...copyParams,
          success: (resp) => {
            resolve(resp);
          },
          fail: (err) => {
            reject(err);
          },
        });
      });
    }
    return new Promise((resolve, reject) => {
      const pendingRequest = {
        action() {
          Network.request({
            ...params,
            success: (res) => {
              resolve(res);
            },
            error: (err) => {
              reject(err);
            },
          });
        },
      };
      pendingRequestQueue.push(pendingRequest);
    });
  },
  login() {
    return new Promise((resolve, reject) => {
      // 登录
      if (User.isPreLogin()) {
        uni
          .checkSession()
          .then(() => {
            //已完成预登陆,有userId和token,session也未过期,可以正常访问小程序
            resolve();
          })
          .catch((e) => {
            doLogin({}).then(() => {
              resolve();
            });
          });
      } else {
        doLogin({}).then(() => {
          resolve();
        });
        console.log('login');
      }
    });
  },
  async authorize(data) {
    let res = await Network.request({
      url: '/api/1.0/user/authorize',
      data: data,
      method: 'POST',
    });
    return res;
  },
  async getBanners(data) {
    return await Network.request({
      url: '/api/1.0/banners',
      method: 'GET',
      data,
    });
  },
  async getStoreLocation(data) {
    return await Network.request({
      url: '/api/1.0/store-distance',
      method: 'get',
      data,
    });
  },
  async getCommodityCategories(data) {
    return await Network.request({
      url: '/api/1.0/commodity/cate',
      method: 'get',
      data,
    });
  },
  async getUserAddressList(data) {
    return await Network.request({
      url: '/api/1.0/address/index',
      method: 'get',
      data,
    });
  },
  async createUserAddress(data) {
    return await Network.request({
      url: '/api/1.0/address/create',
      method: 'post',
      data,
    });
  },
  async getUserAddressDetail(data) {
    return await Network.request({
      url: '/api/1.0/address/detail',
      method: 'get',
      data,
    });
  },
  async updateUserAddress(data) {
    return await Network.request({
      url: '/api/1.0/address/update',
      method: 'post',
      data,
    });
  },
  async decryptedPhone(data) {
    return await Network.request({
      url: '/api/1.0/user-phone/decode',
      method: 'post',
      data,
    });
  },
  async checkOrderStatus(data) {
    return await Network.request({
      url: '/api/1.0/order/status',
      method: 'get',
      data,
    });
  },
  async createOrder(data) {
    return await Network.request({
      url: '/api/1.0/commodity-order/create',
      method: 'post',
      data,
    });
  },
  async getCurrentOrders(data) {
    return await Network.request({
      url: '/api/1.0/commodity-order/current',
      method: 'get',
      data,
    });
  },
  async getHistorytOrders(data) {
    return await Network.request({
      url: '/api/1.0/commodity-order/history',
      method: 'get',
      data,
    });
  },
  async getOrderDetail(data) {
    return await Network.request({
      url: '/api/1.0/commodity-order/detail',
      method: 'get',
      data,
    });
  },
  async getOrderPayConfig(data) {
    return await Network.request({
      url: '/api/1.0/commodity-order/pay',
      method: 'post',
      data,
    });
  },
  async userProfile(data) {
    return await Network.request({
      url: '/api/1.0/user/profile',
      method: 'get',
      data,
    });
  },
  async storeUserProfile(data) {
    return await Network.request({
      url: '/api/1.0/user/profile',
      method: 'post',
      data,
    });
  },
  async getUnreadCoupon() {
    return await Network.request({
      url: '/api/1.0/coupon/unread',
      method: 'get',
    });
  },
  async getEnableCouponList(data) {
    return await Network.request({
      url: '/api/1.0/coupon/enable-count',
      method: 'post',
      data,
    });
  },
  async getMyCouponCount() {
    return await Network.request({
      url: '/api/1.0/coupon/count',
      method: 'get',
    });
  },
  async getMyScoreCount() {
    return await Network.request({
      url: '/api/1.0/score/count',
      method: 'get',
    });
  },
  async getCouponDetail(data) {
    return await Network.request({
      url: '/api/1.0/coupon/detail',
      method: 'get',
      data,
    });
  },
  async getCouponList(data) {
    return await Network.request({
      url: '/api/1.0/coupon/enable-list',
      method: 'get',
      data,
    });
  },
  async getPickUpRangeTime() {
    return await Network.request({
      url: '/api/1.0/pick-up-time',
      method: 'get',
    });
  },
  async getRechargeCards() {
    return await Network.request({
      url: '/api/1.0/recharge-cards',
      method: 'get',
    });
  },
  async submitRechargeInfo(data) {
    return await Network.request({
      url: '/api/1.0/recharge-cards/submit',
      method: 'post',
      data,
    });
  },
  async getUserWalletRecords(data) {
    return await Network.request({
      url: '/api/1.0/recharge-cards/history',
      method: 'get',
      data,
    });
  },
  async getArticleList(data) {
    return await Network.request({
      url: '/api/1.0/article/list',
      method: 'get',
      data,
    });
  },
};

export function doLogin(data) {
  return new Promise((resolve, reject) => {
    uni.login({
      success(res) {
        console.log(res);
        const code = res.code;
        let params = { code };
        const fromUserId = uni.getStorageSync('fromUserId');
        params.invite_user_id = fromUserId ? fromUserId : '';
        if (data.type) {
          params.type = data.type;
          params.user_id = data.userId;
        }
        Network.request({
          url: '/api/1.0/login',
          data: params,
          method: 'POST',
        }).then((resp) => {
          console.log(resp);
          if (resp.data.code == 200) {
            const serverData = resp.data.data;
            console.log('=========', resp.data.data.user);
            let user = new User();
            user.parseJson(resp.data.data.user);
            User.currentUser = user;
            User.token = serverData.token;
            uni.$emit(Network.event.loginSuccess);
            resolve();
          }
        });
      },
    });
  });
}

uni.$on(Network.event.loginSuccess, () => {
  console.log('event.loginSuccess triggerred');
  pendingRequestQueue.forEach((pendingRequest) => {
    pendingRequest.action();
  });
  pendingRequestQueue = [];
});
