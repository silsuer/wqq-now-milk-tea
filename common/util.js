function formatTime(time) {
  if (typeof time !== 'number' || time < 0) {
    return time;
  }

  var hour = parseInt(time / 3600);
  time = time % 3600;
  var minute = parseInt(time / 60);
  time = time % 60;
  var second = time;

  return [hour, minute, second]
    .map(function (n) {
      n = n.toString();
      return n[1] ? n : '0' + n;
    })
    .join(':');
}

function formatDateTime(date, fmt = 'yyyy-MM-dd hh:mm:ss') {
  if (!date) {
    return '';
  }
  if (typeof date === 'number') {
    date = new Date(date * 1000);
  }
  var o = {
    'M+': date.getMonth() + 1, //月份
    'd+': date.getDate(), //日
    'h+': date.getHours(), //小时
    'm+': date.getMinutes(), //分
    's+': date.getSeconds(), //秒
    'q+': Math.floor((date.getMonth() + 3) / 3), //季度
    S: date.getMilliseconds(), //毫秒
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(
      RegExp.$1,
      (date.getFullYear() + '').substr(4 - RegExp.$1.length)
    );
  for (var k in o)
    if (new RegExp('(' + k + ')').test(fmt))
      fmt = fmt.replace(
        RegExp.$1,
        RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length)
      );
  return fmt;
}

const getLocationPermission = () => {
  return new Promise((resolve, reject) => {
    uni.getSetting({
      success(res) {
        console.log(res);
        if (res.authSetting['scope.userLocation']) {
          resolve();
        }
        if (res.authSetting['scope.userLocation'] === false) {
          uni.showModal({
            content: '检测到您没打开奉茶NOW的定位权限，是否去设置打开？',
            confirmText: '确认',
            cancelText: '取消',
            success: function (resp) {
              //点击“确认”时打开设置页面
              if (resp.confirm) {
                uni.openSetting({
                  success: () => {
                    resolve();
                  },
                });
              } else {
                reject();
              }
            },
          });
        }
        if (!res.authSetting['scope.userLocation']) {
          uni.authorize({
            scope: 'scope.userLocation',
            success: () => {
              // 用户已经同意小程序使用地理位置
              resolve();
            },
            fail: () => {
              reject();
            },
          });
        }
      },
    });
  });
};

function formatLocation(longitude, latitude) {
  if (typeof longitude === 'string' && typeof latitude === 'string') {
    longitude = parseFloat(longitude);
    latitude = parseFloat(latitude);
  }

  longitude = longitude.toFixed(2);
  latitude = latitude.toFixed(2);

  return {
    longitude: longitude.toString().split('.'),
    latitude: latitude.toString().split('.'),
  };
}

var dateUtils = {
  UNITS: {
    年: 31557600000,
    月: 2629800000,
    天: 86400000,
    小时: 3600000,
    分钟: 60000,
    秒: 1000,
  },
  humanize: function (milliseconds) {
    var humanize = '';
    for (var key in this.UNITS) {
      if (milliseconds >= this.UNITS[key]) {
        humanize = Math.floor(milliseconds / this.UNITS[key]) + key + '前';
        break;
      }
    }
    return humanize || '刚刚';
  },
  format: function (dateStr) {
    var date = this.parse(dateStr);
    var diff = Date.now() - date.getTime();
    if (diff < this.UNITS['天']) {
      return this.humanize(diff);
    }
    var _format = function (number) {
      return number < 10 ? '0' + number : number;
    };
    return (
      date.getFullYear() +
      '/' +
      _format(date.getMonth() + 1) +
      '/' +
      _format(date.getDate()) +
      '-' +
      _format(date.getHours()) +
      ':' +
      _format(date.getMinutes())
    );
  },
  parse: function (str) {
    //将"yyyy-mm-dd HH:MM:ss"格式的字符串，转化为一个Date对象
    var a = str.split(/[^0-9]/);
    return new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);
  },
};

const hexToRgba = (hex, opacity) => {
  //16进制颜色转rgba
  return (
    'rgba(' +
    parseInt('0x' + hex.slice(1, 3)) +
    ',' +
    parseInt('0x' + hex.slice(3, 5)) +
    ',' +
    parseInt('0x' + hex.slice(5, 7)) +
    ',' +
    opacity +
    ')'
  );
};

const popSubscribeMessage = (types) => {
  // 弹出订阅消息
  let list = [
    'URz0CajIdCtVDbg27uukUOp4Rx36l2oN2ScE90DDs0c', // 0 订单已接收提醒
    'L-FfaK50PDpTDxMuoQ7qcvhr_KSncH0Hj7dh3IF48Cw', // 1 点餐成功
    'DC3Hm6EX7WG8RRv8V39CCJmo3j8S9evBURn2VvNSLZ0', // 2 取餐提醒
    'sIF_We6qv8ssab3jZZFh-ZGrEhK68CrcAFSJSWnI5N8', // 3 取消订单通知
    'BuIR-detExz34s96bnsvJF9jWU3VQuCnTvL2rj-iaHs', // 4 订单状态变更通知
    'FxVhCZfWwJhnKHGU3I9vNkXoh5FzaDtD25p9ESn62ic', // 5 充值成功通知
  ];
  let temp = [];
  for (let i = 0; i < types.length; i++) {
    temp.push(list[types[i]]);
  }
  return new Promise((resolve) => {
    uni.requestSubscribeMessage({
      tmplIds: temp,
      success: function (res) {
        console.log(res);
        resolve();
      },
    });
  });
};

module.exports = {
  formatTime,
  formatDateTime,
  formatLocation,
  dateUtils,
  hexToRgba,
  getLocationPermission,
  popSubscribeMessage,
};
