let env = 1 //0开发，1生产

let serverHost = ''
let wssHost = 'wss://wss.techub.tech/wss'
let debug = false
if (env == 0) {
	debug = true
	serverHost = 'http://wqq-tea.test';
	wssHost = 'ws://127.0.0.1:8282'
} else if (env == 1) {
	debug = true
	serverHost = 'https://wqq.techub.tech'
}
const AppConfig = {
	version: '1.2.1',
	serverHost,
	wssHost,
	debug
}

export default AppConfig
