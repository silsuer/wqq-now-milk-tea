import AppConfig from '../config';

var User_Storage_key = AppConfig.debug ? 'test.User' : 'User';
const User_Token_Storage_key = AppConfig.debug
  ? 'test.User.token'
  : 'User.token';

export class User {
  static event = {
    profileUpdated: 'profileUpdated',
  };

  id;
  unionId;
  profile;

  static get currentUser() {
    var value = uni.getStorageSync(User_Storage_key);
    if (value != null) {
      var user = new User();
      user.parseJson(value);
      return user;
    }
    return null;
  }
  static set currentUser(user) {
    if (user) {
      uni.setStorageSync(User_Storage_key, user.toJson());
      uni.$emit(User.event.profileUpdated);
    } else {
      uni.removeStorageSync(User_Storage_key);
    }
  }

  static get token() {
    let value = uni.getStorageSync(User_Token_Storage_key);
    if (value != null && value.length > 0) {
      return value;
    }
    return null;
  }

  static set token(token) {
    if (token) {
      uni.setStorageSync(User_Token_Storage_key, token);
    } else {
      uni.removeStorageSync(User_Token_Storage_key);
    }
  }

  static isPreLogin() {
    const user = User.currentUser;
    const token = User.token;
    console.log(user, token, '============');
    if (user && user.id && token) {
      return true;
    }
    return false;
  }

  toJson() {
    let json = {
      id: this.id,
      profile: this.profile
      // profile: this.profile.toJson(),
      // extend: this.extend.toJson(),
      // fans_count: this.fansCount,
      // follows_count: this.followsCount,
      // collects_count: this.collectsCount,
      // is_blacked: this.isBlacked,
      // is_verified: this.isVerified,
      // permission_roles: permissionRoles,
      // verified: this.verified.toJson(),
      // attendance_count: this.attendanceCount,
      // college: this.college.toJson(),
      // college_id: this.collegeId,
      // verify_type: this.verifiedType,
      // verify_name: this.verifiedName,
      // verified_icon_url: this.verifiedUrl
    };
    return json;
  }

  parseJson(json) {
    this.id = json.id;
    this.profile = json.profile;
  }
}
